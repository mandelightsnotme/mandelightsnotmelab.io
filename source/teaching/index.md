---
title: Teaching
date: 2018-02-28 23:38:25
thumbnail: teaching.jpg
---

Whether I'm in the classroom or on the stage, my teaching philosophy leads me to connect what we learn with how we engage with the world. At Syracuse University, I work with the __[LGBT Resource Center](http://lgbt.syr.edu/)__ and the __[Pronouns, Gender, and Preferred Name Advisory Council](https://answers.syr.edu/display/UCC/PGPNAC+-+Pronoun%2C+Gender%2C+and+Preferred+Name+Advisory+Council)__ to design and deliver diversity and inclusion trainings for professional development; I also teach classes on writing, gender, race, and English literature for the __[College of Arts and Sciences](https://thecollege.syr.edu/english-department/english-graduate-programs/ma-and-phd-students/o-mara-ashley/)__. In the community arts scene, I have worked with __[The Redhouse](http://www.theredhouse.org/)__ to lead audience conversations on theatrical performances.  I practice a feminist pedagogy: my teaching style aims to level hierarchies in the classroom. This way, I learn as much from my students as my students learn from me, as I mentor them from wherever they're at to wherever they want to go.

### Diversity and Inclusion

<img style="float:left;margin-right:20px;margin-bottom:20px" src="pronouns.jpg" /> __[Pronouns Competency Workshop](https://answers.syr.edu/display/UCC/PGPNAC+-+Pronoun%2C+Gender%2C+and+Preferred+Name+Advisory+Council)__, Syracuse University (ongoing).
I co-lead 60-90 minute sessions on the role of pronouns and preferred names as a part of LGBTQ inclusion and cultural sensitivity. We consider the function and role of gender in our lives, practice using pronouns in common situations, and learn how to increase accurate personal pronoun usage in our professional practice.
<div style="clear:both"></div>


### Community Arts

<img style="float:left;margin-right:20px;margin-bottom:20px" src="passion.jpg" /> __Talkback to [Sarah Ruhl's _Passion Play_](https://www.syracusenewtimes.com/redhouse-mines-ruhls-gold/)__, _The Redhouse_, 9 April 2016.
"Hailed by the New Yorker's John Lahr as 'extraordinary,' 'bold,' and 'inventive,' Passion Play takes us behind the scenes of three communities attempted to stage the death and resurrection of Christ. From Queen Elizabeth's England to Hitler's Germany to Reagan's America, Ruhl's exploration of devotion takes us on a humorous yet unsettling journey filled with lust, whimsy, and a lot of fish."

I led an after-performance audience discussion on early-modern religion and theater, and its enduring impact on the politics of Christianity.
<div style="clear:both"></div>

<img style="float:left;margin-right:20px;margin-bottom:20px" src="midsummer.jpg" /> __Talkback to [William Shakespeare's _A Midsummer Night's Dream_](https://www.syracusenewtimes.com/double-duty-in-the-dacks-for-redhouse-rep-cast/)__, _The Redhouse_, 13 February 2016.
"Hermia loves 'bad boy' Lysander, but her father wants her to marry Demetrius, who’s also the heartthrob of her best-friend-forever, Helena. Threatened with death or a convent if she doesn’t do what Daddy wants, Hermia and Lysander head for the woods. With Helena and Demetrius in hot pursuit, they—and some well-meaning, artistically challenged local Thespians—run right into a magical free-for-all between Lumberjack Oberon, the Fairy King, and Hippy Titania, his Fairy Queen. It’s a wild night for lovers and lunatics, swirling with Adirondack inspired flourishes, in this family-friendly comedy."

I led an after-performance audience discussion on early-modern marriage, gender, and sexuality.
<div style="clear:both"></div>
### Classroom Instruction

<img style="float:left;margin-right:20px;margin-bottom:20px" src="classroom.jpg" />I have developed lower-division college courses on composition through the lens of gender in world religions; a survey of British Literature, Beginnings to 1789; Race and Literary Texts; and Gender and Literary Texts. I also give guest lectures on subjects related to my research.

- __Race and Colonialism in _The Tempest___, ETS 121: Introduction to Shakespeare (Evan Hixon), Syracuse University, 12 April 2018.
- __Gender and Sexuality in _Hamlet___, ETS 121: Introduction to Shakespeare (Evan Hixon), Syracuse University, 27 March 2018.
- __Queer Representation in the BBC _Sherlock_ Fandom__, ETS 145: Reading Popular Culture (Rhyse Curtis), Syracuse University, 15 November 2017.

__First Day Roster Form__

In order to get to know my students and how I can help best teach them, I created this form that all students complete on the first day or when they add my course. Highlights include asking for students' pronouns and content warnings, for what their interest is in the class, and for what their expectations are for themselves and the instructor.

__[Download the roster form](/teaching/first-day-roster-form.pdf)__.
<div style="clear:both"></div>
