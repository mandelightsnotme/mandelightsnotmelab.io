---
title: Early-modern asexuality roundtable
date: 2020-10-15 17:00:00
thumbnail: /about/acmrs.png
tags:
    - theater
    - shakespeare
    - queer
    - news
---
Thus far, there has been little-to-no work in premodern asexuality studies, even as asexual identity, theory, and scholarship are at the forefront of contemporary gender and sexuality studies. This roundtable, the first of its kind, seeks to change that.

I am immensely grateful to the Arizona Center for Medieval and Renaissance Studies (ACMRS) for adopting this roundtable that I co-convened with Simone Chess. We couldn't share it at RSA this year, but presenting it online for ACMRS's audience more than made up for it.

**[Watch the full presentation on Youtube](https://youtu.be/9vSgG02fH6M).**

<iframe width="560" height="315" src="https://www.youtube.com/embed/9vSgG02fH6M" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<div style="clear:both"></div>
