---
title: New essay at Howlround
date: 2019-09-10 17:00:00
thumbnail: /about/jenet-hamlet-full.jpg
tags:
    - theater
    - media
    - queer
    - news
---
 "It’s a queer actor’s—and audience’s—dream: to be treated as unremarkable; to have one’s experiences represented as they are without patronizing repackaging for unfamiliar audiences."

 Check out my latest for _Howlround_, a hybrid review/interview with Jenet Le Lacheur, the transfeminine nonbinary actor who starred in Iris Theatre's 2019 _Hamlet_:

 __["Unmanly Grief: Performing A Trans Hamlet"](https://howlround.com/unmanly-grief)__.
