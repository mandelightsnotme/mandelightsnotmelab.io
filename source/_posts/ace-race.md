---
title: Race and Asexuality Studies
date: 2020-09-09 20:00:00
thumbnail: /about/blm-ace-flag.png
tags:
    - asexuality
    - research
    - queer
    - news
---
Asexuality studies replicates much of the same racism of asexuality as a movement — which replicates much of the same racism of the broader queer movement.

(a post in honor of #ScholarStrike)

It's too late for asexuality studies to be anti-racist from the beginning, but the field is still in its infancy. It is the responsibility of scholars working in the field to uproot that racism now, before it grows any deeper.

To be clear about my positionality: I identify with asexuality (this phrasing is deliberate and something I will return to). I work in asexuality studies from early-modern literary studies. I am mixed race/ethnicity, white and Arab, moving through the world with the privilege of white skin and a white name. My training is not in anti-racism. Nevertheless it is my responsibility as a scholar (and a person) to retrain myself in anti-racism, for the good of my students, my discipline, and my communities. My dissertation does not focus on race *because* asexuality studies has not focused on race. But I take seriously David Sterling Brown's exhortation to examine Shakespeare's "other race plays" because no text, concept, or ideology is race-neutral. Every Shakespeare play is a race play. When asexuality studies does not explicitly focus on race, it effaces the whiteness of the field, and the whiteness of how the asexual movement has emerged and developed.

The scholarly discourse of asexuality has largely been borrowed unquestioningly from AVEN (a point made by Evelyn Elgie in their illuminating master's thesis). AVEN, as Black scholar Brittney Miles point out, positions itself as "colorblind." If you're unfamiliar with the organizations of the asexual movement: AVEN is the asexual HRC. They've done some work to "diversify" their board, but their mission is "visibility" and "education," not liberation. The prevailing voices at the beginning of the contemporary asexual movement were white, and their whiteness informed the discourse of the asexual movement as we know it today. When asexuality studies imports that same discourse, it imports that same whiteness. What, then, are the implicit assumptions of asexuality studies and the asexual movement? What is missing from both, in their implicit whiteness? This is a subject that deserves a lot of serious inquiry. For the sake of this post, I'll sketch out a few observations.

* As [Michael Paramo](https://azejournal.com/mxparamo) has often pointed out, the discourse of sexuality — hetero-, homo-, bi-, a-, and otherwise — is the product of colonization and white supremacy. Not interrogating that whiteness limits our scholarship.
* As yingchen and yingtong, the creators of "[an aromantic manifesto](https://aromanticmanifesto.tumblr.com/)," point out, the discourse of romance (and to a great extent sexuality) is capitalist, white supremacist, cissexist, heterosexist, sizeist, and ableist. Not interrogating that background limits our scholarship. Western ideas about who counts as partners, family, and kin are shaped by white, colonial, anthropocentric ideologies.
* Not accounting for non-white modes of kinship when we discuss queerplatonic and polyamorous relationships limits our scholarship. (Kim TallBear and Leanne Betasamosake Simpson don't work on asexuality, but the work they each do on indigenous modes of kinship is instructive — hat tip to Alok Vaid-Menon and and Elgie respectively.)
* Who gets to experience sexual desire — and who gets to be the object of it — and on what terms is strongly inflected by white supremacy. Check out the work of [Ianna Hawkins-Owen](http://iannahawkinsowen.com/) (Blackness and asexuality) and [Alok Vaid-Menon](https://mediadiversified.org/2014/05/03/whats-race-got-to-do-with-it-white-privilege-asexuality/) (South Asian brownness and asexuality).
* And from within early modern studies — how can we study the birth of marriage culture and Protestant modes of compulsory sexuality without accounting for its connections to the formation of a white English national identity?

With the exception of Elgie, all the scholars that I refer to here are scholars of color. They're not all traditional scholars, either — not everyone in here has a PhD or is connected to an academic institution. Gatekeeping what counts as scholarship is also racism.

So when I say I identify with asexuality, what I mean is that I recognize in the movement elements that are useful for combatting the compulsory sexuality that I actively disidentify with. But asexuality is not the only tool for dismantling compulsory sexuality — especially when the asexual movement is complicit in the white supremacy that shapes heterosexism.
