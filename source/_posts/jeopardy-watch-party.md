---
title: Jeopardy! Watch Party
date: 2018-03-09 23:38:25
thumbnail: /about/omara-jeopardy.jpg
tags:
    - Jeopardy
    - media
    - events
    - news
---
Are you in Syracuse? Come out to the watch party of my episode of ___Jeopardy!___ We'll gather at the __[Sitrus at the Sheraton](http://www.sheratonsyracuse.com/syracuse-restaurants)__ on __Monday, March 12, 2018, at 6:30p.m.__. We'll play trivia and I'll talk about the experience of auditioning for and recording _Jeopardy_. At __7:30p.m.__, we'll watch the episode live! More details on the event on __[Facebook](https://www.facebook.com/events/2014541152200020/)__.

Not in Syracuse? Check your local listings for time and station information!
