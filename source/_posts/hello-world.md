---
title: Welcome!
date: 2018-02-28 23:38:25
thumbnail: /media/protest.jpg
tags:
    - about
    - commissions
    - consulting
---
Welcome to my professional website. Here I have archived my published writing, my talks and presentations, and my public scholarship.

I am available for __[writing commissions](/writing)__, __[invited lectures](/speaking)__ and invitations to __[pitch articles](/writing)__, __[research consultation](/research)__, and requests for __[interviews](/research)__. More information is available on their respective pages on my website.
