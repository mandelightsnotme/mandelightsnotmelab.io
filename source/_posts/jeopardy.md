---
title: Jeopardy Air Date
date: 2018-03-08 23:38:25
thumbnail: /about/omara-jeopardy.jpg
tags:
    - Jeopardy
    - media
    - events
    - news
---
At long last, I will be appearing on ___Jeopardy!___ on __Monday, March 12, 2018__. Check your local listings for time and station information. Also, keep an eye on Syracuse media — I'll be giving a few interviews in print and on television!

In Syracuse but can't access WSYT? Stay tuned for information about a public watch party.
