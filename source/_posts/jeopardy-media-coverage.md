---
title: Jeopardy! Media Roundup 1
date: 2018-03-18 23:38:25
thumbnail: /about/omara-jeopardy.jpg
tags:
    - Jeopardy
    - media
    - events
    - news
---
I'm overwhelmed by the media coverage of my appearance on ___Jeopardy!___

- __Syracuse University__ __[interviewed](https://news.syr.edu/2018/03/grad-student-to-appear-on-jeopardy/)__ me about the experience.
- __The Post-Standard/Syracuse.com__ __[reported](http://www.syracuse.com/entertainment/index.ssf/2018/03/syracuse_jeopardy_contestant.html)__ on the interview, revived all my old articles and winning short stories, and resurrected their photo of thirteen-year-old Ashley.
- __CNY Central__ filmed an __[interview](http://cnycentral.com/news/local/su-instructor-competes-in-upcoming-jeopardy-episode)__ with me.
- __96.1 The Eagle__ __[blogged](http://961theeagle.com/syracuse-phd-student-on-jeopardy-monday-night/)__ about my appearance.
- __WIBX 950 (Utica)__ invited me on the air for two interviews; here's a __[recording](https://soundcloud.com/user-236881114/ashleyjeopardyradiocut)__ of the first one.
- __Onondaga Community College__ put a __[profile](https://news.sunyocc.edu/2018/03/12/occ-alum-appears-on-jeopardy/)__ about me on their homepage.
- __The NewsHouse__, Newhouse's student-run news, published my __[favorite interview](https://www.thenewshouse.com/story/syracuse-university-doctoral-student-appear-jeopardy)__.
