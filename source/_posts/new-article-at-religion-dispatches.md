---
title: New article at Religion Dispatches
date: 2018-08-28 17:00:00
thumbnail: /about/wmf-large.jpg
tags:
    - Catholic
    - media
    - events
    - queer
    - news
---
 "Like an abusive parent, the Catholic Church dismisses our real needs and our holy talents until they seem irrelevant, all while reinforcing its own authority."

 Check out my latest for _Religion Dispatches_, a reaction to Fr. James Martin, SJ.'s talk on "How Parishes Can Welcome LGBT Catholics":

 __["World Meeting of Families again positions queer Catholics as children"](https://rewire.news/religion-dispatches/2018/08/28/world-meeting-of-families-again-positions-queer-catholics-as-children/)__.
