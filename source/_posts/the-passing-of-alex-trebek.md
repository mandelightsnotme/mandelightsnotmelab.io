---
title: The passing of Alex Trebek
date: 2018-03-18 23:38:25
thumbnail: /about/omara-jeopardy.jpg
tags:
    - Jeopardy
    - media
    - events
    - news
---
Alex Trebek, longtime host of ___Jeopardy!___, passed away this weekend. Several local news outlets asked me, as someone who hqs met him, for my thoughts.

- __The Post-Standard/Syracuse.com__ __[reported](https://www.syracuse.com/living/2020/11/what-cnys-most-memorable-jeopardy-contestants-remember-about-alex-trebek-photos.html)__ interviewed me shortly after Trebek's cancer diagnosis was made public.
- __CNY Central__ filmed an __[interview](http://cnycentral.com/news/local/su-instructor-competes-in-upcoming-jeopardy-episode)__ with me.
- __96.1 The Eagle__ __[blogged](http://961theeagle.com/syracuse-phd-student-on-jeopardy-monday-night/)__ about my appearance.
- __WIBX 950 (Utica)__ invited me on the air for two interviews; here's a __[recording](https://soundcloud.com/user-236881114/ashleyjeopardyradiocut)__ of the first one.
- __Onondaga Community College__ put a __[profile](https://news.sunyocc.edu/2018/03/12/occ-alum-appears-on-jeopardy/)__ about me on their homepage.
- __The NewsHouse__, Newhouse's student-run news, published my __[favorite interview](https://www.thenewshouse.com/story/syracuse-university-doctoral-student-appear-jeopardy)__.
