---
title: New essay at Howlround
date: 2020-02-20 17:00:00
thumbnail: /about/djcorey-everybody.png
tags:
    - theater
    - media
    - queer
    - news
---
 "As the Usher tells the audience: the idea that these actors are, in their own bodies and identities, each equally suited to all of the many different roles 'destabilize[s] your preconceived notions about identity.'."

 Check out my latest for _Howlround_, a hybrid review/interview with Will Davis, Alina Collins Maldonado, Avi Roque, and Kelli Simpkins on Davis' production of _Everybody_ (Shakespeare Theatre Company, 2019) — Branden Jacobs-Jenkins' modern adaptation of the medieval morality play _Everyman_:

 __["Something for _Everybody_: Radical Inclusion in a Modern Adaptation"](https://howlround.com/something-everybody)__.
