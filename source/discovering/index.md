---
title: Discovering
date: 2018-02-28 23:38:25
thumbnail: discovering.jpg
---

My dissertation examines literary representations of celibacy after the English Reformation; I look at how Catholicism’s institutional celibacy shaped anti-Catholic rhetoric at the same time that it created a space to articulate non-normative asexual desires.

More broadly, I'm invested in recent developments in feminism, asexuality, and queer studies, especially gender and religion. I have presented some of my academic research in my published __[writing for the public](../writing)__. I also have ongoing research projects in the digital humanities, using data visualization to trace networks and examine trends in the production of texts.

### Digital Humanities

<img style="float:left;margin-right:20px;margin-bottom:20px" src="sdfb.jpg" alt="clusters of labeled dots connected by lines" /> __[Six Degrees of Francis Bacon](http://www.sixdegreesoffrancisbacon.com/)__
I am a curator and contributor to the SDFB database visualizing relationships among early-modern people who lived in England. I am responsible for over two dozen additions of persons, relationships, and group assignments to the database, specializing in English Jesuit missionary networks. The first-level network of members of the Society of Jesus is pictured to the left.
<div style="clear:both"></div>

<img style="float:left;margin-right:20px;margin-bottom:20px" src="fanfic.jpg" alt="a bar graph representing the quantity of quantity of ace fic in different fandoms" /> __Asexuality in Fanfic__
I am interested in trends in the distribution and popularity of representations of asexuality in fanfic in the fanfic database __[Archive of Our Own](https://archiveofourown.org)__ (AO3). This is an ongoing project focusing on trends in the BBC _Sherlock_ fandom, the fandom with the greatest gross number of fics featuring asexuality (and the second-greatest relative to non-ace fics in the fandom) in the archive.
<div style="clear:both"></div>
