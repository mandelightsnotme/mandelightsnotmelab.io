---
title: Writing
date: 2018-02-28 23:38:25
thumbnail: writing.jpg
---

I write about queer sexuality and gender, politics, and theater. I have published essays and articles with  ___[Howlround](https://howlround.com/)___, ___[America Magazine](http://americamagazine.org)___, ___[Religion Dispatches](religiondispatches.org)___, and ___[Broadly Textual](https://broadlytextual.com/)___.

### Theater
<img style="float:left;margin-right:20px;margin-bottom:20px" src="djcorey-everybody.png" /> __["Something for _Everybody_: Radical Inclusion in a Modern Adaptation"](https://howlround.com/something-everybody)__, Howlround_, 20 February 2020.
"As the Usher tells the audience: the idea that these actors are, in their own bodies and identities, each equally suited to all of the many different roles 'destabilize[s] your preconceived notions about identity.'." *(Photo by DJ Corey, courtesy of Shakespeare Theatre Company)*
<div style="clear:both"></div>

<img style="float:left;margin-right:20px;margin-bottom:20px" src="jenet-hamlet.jpg" /> __[Unmanly Grief: Performing A Trans Hamlet](https://howlround.com/unmanly-grief)__, _Howlround_, 10 September 2019.
"It’s a queer actor’s—and audience’s—dream: to be treated as unremarkable; to have one’s experiences represented as they are without patronizing repackaging for unfamiliar audiences." *(Image courtesy of Iris Theatre)*
<div style="clear:both"></div>

### Religion and Spirituality
<img style="float:left;margin-right:20px;margin-bottom:20px" src="wmf.jpg" /> __[World Meeting of Families again positions queer Catholics as children](https://religiondispatches.org/world-meeting-of-families-again-positions-queer-catholics-as-children/)__, _Religion Dispatches_, 28 August 2018.
"Like an abusive parent, the Catholic Church dismisses our real needs and our holy talents until they seem irrelevant, all while reinforcing its own authority."
<div style="clear:both"></div>


<img style="float:left;margin-right:20px;margin-bottom:20px" src="fasting.jpg" /> __[Fasting, fat shaming and finding Christ in my body](https://www.americamagazine.org/faith/2018/02/07/fasting-fat-shaming-and-finding-christ-my-body)__, _America Magazine_, 7 February 2018.
"People like me have unconsciously learned to hate the bodies God created because they do not meet the standard created by society."
<div style="clear:both"></div>

<img style="float:left;margin-right:20px;margin-bottom:20px" src="lost.jpg" /> __[How I misplaced my faith](https://broadlytextual.com/2017/10/06/special-edition-how-i-misplaced-my-faith/)__, _Broadly Textual_, 6 October 2018.
"I left it in the run-down Amtrak station in Schenectady, New York: a tiny room with a manual train schedule, a contaminated drinking fountain, and an air freshener that whined every quarter hour."
<div style="clear:both"></div>

### Asexuality
<img style="float:left;margin-right:20px;margin-bottom:20px" src="ace.jpg" /> __Asexuality as difference__, _Broadly Textual_, November 2017.
"The problem with asexuality, as I’ve discussed before, is that it is hard to talk about on its own terms — even in a grammatical sense. But what if that weren’t the case?"
[Misrepresenting Difference: Objectifying Asexuality in Journalism](https://broadlytextual.com/2017/11/03/misrepresenting-difference-objectifying-asexuality-in-journalism/)
[Abnormalizing Difference: Sexual Normativity in Asexual Sherlock Fanfic](https://broadlytextual.com/2017/11/10/abnormalizing-difference-sexual-normativity-in-asexual-sherlock-fanfic/)
[Normalizing Difference: Redefining Asexuality](https://broadlytextual.com/2017/11/17/normalizing-difference-redefining-asexuality/)
[Valuing Difference: An Ace on Food, Friendship, and Fluffy Companionship](https://broadlytextual.com/2017/12/01/valuing-difference-an-ace-on-food-friendship-and-fluffy-companionship/)
<div style="clear:both"></div>

### Queer History
<img style="float:left;margin-right:20px;margin-bottom:20px" src="history.jpg" /> __A History of LGBT History__, _Broadly Textual_, October 2014.
"Over the next few weeks, I will look at how we 'do' LGBT and queer histories from a literary perspective: what it means to (re)construct a queer history, and the advantages and pitfalls of identifying queer figures in literary history."
[A History of LGBT History](https://broadlytextual.com/2014/10/03/a-history-of-lgbt-history/)
[Recuperation as Resistance: The Icons of LGBT History](https://broadlytextual.com/2014/10/10/recuperation-as-resistance-the-icons-of-lgbt-history/)
[Overwriting History: “Just Reading” and the Case of John Henry Newman](https://broadlytextual.com/2014/10/17/overwriting-history-just-reading-and-the-case-of-john-henry-newman/)
[Queering LGBT History: The Case of Sherlock Holmes Fanfic](https://broadlytextual.com/2014/10/24/queering-lgbt-history-the-case-of-sherlock-holmes-fanfic/)
[Coda: Asexual Awareness Week and the Future of Queer Theory](https://broadlytextual.com/2014/10/31/coda-asexual-awareness-week-and-the-future-of-queer-theory/)
<div style="clear:both"></div>

### Early-modern Catholicism
<img style="float:left;margin-right:20px;margin-bottom:20px" src="recusancy.jpg" /> __Theater and Conversion__, _Broadly Textual_, January 2017.
"Anyone that could play the turncoat once was presumed to be inclined to do it again. One never fully shook their former identity."
[Un/natural Citizens: Naturalization and Conversion](https://broadlytextual.com/2017/01/06/unnatural-citizens-naturalization-and-conversion/)
[Persuasive Performance: Theater and Conversion](https://broadlytextual.com/2017/01/13/persuasive-performance-theater-and-conversion/)
[Legalizing Repression: “Muslim Registries” and English Recusants](https://broadlytextual.com/2017/01/28/legalizing-repression-muslim-registries-and-english-recusants/)
[Coda: Converting Art — Literature During Political Repression](https://broadlytextual.com/2017/02/04/coda-converting-art-literature-during-political-repression/)
<div style="clear:both"></div>
