---
title: Speaking
date: 2018-02-28 23:38:25
thumbnail: speaking.jpg
---
The ideas created in the university belong in the public. This belief leads me to share my scholarship in an accessible medium with local audiences.

### Public Speaking
I am a regular invited speaker at the __[Adult Forum](http://www.theucf.org/ministries/adult-education/)__ at the United Church of Fayetteville, "a place where education, understanding and discussion of complex contemporary topics can occur." I have also participated on panels about writing scholarship for the public and about Catholic spirituality for teens.

<img style="float:left;margin-right:20px" src="confirmation.jpg" alt="a midcentury modern brick and glass building with the words 'John G. Alibrandi Jr. CATHOLIC CENTER' on it" /> __“Discipleship”__
Confirmation Retreat, Alibrandi Catholic Center, 26 March 2017

A personal reflection and motivational talk about social justice for Catholic teens.
<div style="clear:both"></div>

<img style="float:left;margin-right:20px" src="augustine.jpg" alt="a white paneled room with green carpet, a crooked projector screen, and a woman in a red sweater and scarf talking with her hands" /> __“‘Give Me Chastity But Not Yet’: Augustine, Sex, and Sin”__
UCF Adult Forum, 12 March 2017

Part of the "Sex, Sin, and Salvation: Giving Up Guilt for Lent" ecumenical lecture series.
<div style="clear:both"></div>

<img style="float:left;margin-right:20px" src="queering.jpg" alt="" /> __“Queer(ing) Catholics in English Literature”__
UCF Adult Forum, 3 April 2016

Part of the "Religion, Literature, and the Arts" ecumenical lecture series.
<div style="clear:both"></div>

<img style="float:left;margin-right:20px" src="metathesis.jpg" /> __“_Metathesis_ Roundtable: Feminism, Gender, and Public Scholarship”__
Syracuse University, 24 April 2015

A roundtable discussion with the contributors to the first year of ___[Metathesis](https://metathesisblog.com/)___ public scholarship posts, all on the themes of feminism and gender.
</div>

### Conference Presentations
I am a frequent participant in scholarly conferences, sharing my research on religion and sexuality in both early-modern and more recent literature.

<img style="float:left;margin-right:20px" src="conference.jpg" />- __Rib-bed for Her Pleasure: Reproduction and Conversion in the Creation of Eve__
Seminar paper, Objects of Conversion conference, University of California, Los Angeles, 16 February 2017
- __“How art thou changed!”: Transfiguring the Magdalene in Early Modern Converts’ Poetry__
Conference paper, Transforming Bodies conference, Cornell University, 21 April 2017
- __“Divided by Christ”: Synecdoche and Sectarianism in Seventeenth-Century English Protestant Pilgrim Narratives__
Conference paper, Brave New World graduate conference, McGill University, 18 February 2017
- __“El sexo no es escencia en lo entendido”: Sor Juana’s Women’s Bodies and Ungendered Souls__
Conference paper, 2016 NEMLA, Hartford, CT, 20 March 2016
- __“It’s All Fine”: Asexuality and Narratives of Normalcy in the BBC _Sherlock_ Fandom__
Colloquium paper, Fall 2015 Negotiations, Syracuse University, 13 November 2015
- __“Idioma mexicano”: Criollismo and Spanish Text/Paratext in Paredes’s Nahuatl _Catecismo mexicano___
Conference paper, 2014 NEASECS Conference, Syracuse, NY, 26 September 2014
<div style="clear:both"></div>
