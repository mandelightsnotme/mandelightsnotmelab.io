---
title: Speaking
date: 2018-02-28 23:38:25
thumbnail: speaking.jpg
---
The ideas created in the university belong in the public. This belief leads me to share my scholarship in an accessible medium with local audiences.

### Public Speaking


<img style="float:left;margin-right:20px;margin-bottom:20px" src="acmrs.jpg" alt="A Zoom presentation." /> __“Early Modern Asexuality and Performance: An ACMRS Roundtable”__
Arizona Center for Medieval and Renaissance Studies, 15 October 2020

Thus far, there has been little-to-no work in premodern asexuality studies, even as asexual identity, theory, and scholarship are at the forefront of contemporary gender and sexuality studies. This roundtable, the first of its kind, seeks to change that. **[Watch the full presentation on Youtube](https://youtu.be/9vSgG02fH6M).**

<iframe width="560" height="315" src="https://www.youtube.com/embed/9vSgG02fH6M" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<div style="clear:both"></div>



<img style="float:left;margin-right:20px;margin-bottom:20px" src="hamlet.jpg" alt="a still from the Cumberbatch Hamlet: Hamlet grasps Ophelia's head." /> __“Celibacy and Asexuality in English Literature”__
Early Queer Scholarship Series, Syracuse University LGBT Resource Center, 2 April 2019

"Post-Reformation England was a society that insisted on marriage as a universal good and did not, even could not, recognize the feasibility of celibacy as a category."
<div style="clear:both"></div>



<img style="float:left;margin-right:20px;margin-bottom:20px" src="ace.jpg" alt="a graphic of the asexual pride flag: horizontal black, grey, white, and purple stripes." /> __“An Ace Discovers Asexuality History and Culture”__
Human Library, Bird Library, 11 April 2018

What is it like to discover your queer past while living in a straight world? As an ace theorizing asexuality for my doctoral research on early-modern literature, I will share my experience analyzing the history of asexuality and its impact on contemporary ace culture.
<div style="clear:both"></div>


<img style="float:left;margin-right:20px;margin-bottom:20px" src="augustine.jpg" alt="a white paneled room with green carpet, a crooked projector screen, and a woman in a red sweater and scarf talking with her hands" /> __“‘Give Me Chastity But Not Yet’: Augustine, Sex, and Sin”__
[UCF Adult Forum](http://www.theucf.org/ministries/adult-education/), 12 March 2017

Part of the "Sex, Sin, and Salvation: Giving Up Guilt for Lent" ecumenical lecture series.
<div style="clear:both"></div>

<img style="float:left;margin-right:20px;margin-bottom:20px" src="queering.jpg" alt="" /> __“Queer(ing) Catholics in English Literature”__
[UCF Adult Forum](http://www.theucf.org/ministries/adult-education/), 3 April 2016

Part of the "Religion, Literature, and the Arts" ecumenical lecture series.
<div style="clear:both"></div>

<img style="float:left;margin-right:20px;margin-bottom:20px" src="metathesis.jpg" /> __“_Metathesis_ Roundtable: Feminism, Gender, and Public Scholarship”__
Syracuse University, 24 April 2015

A roundtable discussion with the contributors to the first year of ___[Metathesis](https://broadlytextual.com/)___ public scholarship posts, all on the themes of feminism and gender.
<div style="clear:both"></div>

### Conference Presentations
I am a frequent participant in scholarly conferences, sharing my research on sexuality in both early-modern and more recent literature.

<img style="float:left;margin-right:20px;margin-bottom:20px" src="conference.jpg" />
- __Roundtable: Early Modern Asexuality and Performance__
Co-chair, Renaissance Society of America, Philadelphia, PA, 4 April 2020
- __“Married Chastitie”: Asexual Love in “The Phoenix and Turtle” (Part 2)__
Seminar paper, Shakespeare Association of America, Washington, DC, 18 March 2019
- __“Married chastity”: Catholic Celibacy and Platonic Desire in Shakespeare’s “The Phoenix and the Turtle” (Part 1)__
Panel paper, Renaissance Society of America, Toronto, ON, 18 March 2019
- __Rib-bed for Her Pleasure: Reproduction and Conversion in the Creation of Eve__
Seminar paper, Objects of Conversion conference, University of California, Los Angeles, 16 February 2017
- __“How art thou changed!”: Transfiguring the Magdalene in Early Modern Converts’ Poetry__
Conference paper, Transforming Bodies conference, Cornell University, 21 April 2017
- __“Divided by Christ”: Synecdoche and Sectarianism in Seventeenth-Century English Protestant Pilgrim Narratives__
Conference paper, Brave New World graduate conference, McGill University, 18 February 2017
- __“El sexo no es escencia en lo entendido”: Sor Juana’s Women’s Bodies and Ungendered Souls__
Conference paper, 2016 NEMLA, Hartford, CT, 20 March 2016
- __“It’s All Fine”: Asexuality and Narratives of Normalcy in the BBC _Sherlock_ Fandom__
Colloquium paper, Fall 2015 Negotiations, Syracuse University, 13 November 2015
- __“Idioma mexicano”: Criollismo and Spanish Text/Paratext in Paredes’s Nahuatl _Catecismo mexicano___
Conference paper, 2014 NEASECS Conference, Syracuse, NY, 26 September 2014
<div style="clear:both"></div>
