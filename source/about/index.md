---
title: About
date: 2018-02-28 23:38:25
thumbnail: omara-jeopardy.jpg
---

I am a __[freelance writer](/writing)__ and an __[English PhD](https://thecollege.syr.edu/english-department/english-graduate-programs/ma-and-phd-students/o-mara-ashley/)__ candidate at Syracuse University, where I study Shakespeare and queer asexuality as I transition into a career in diversity and inclusion and public arts. I founded __[CNY Aces](https://www.facebook.com/CNY-Aces-105172774338195/)__, a social group for the queer asexual community, I am one of the original co-creators and co-facilitators of the Pronouns Competency Workshop with the __[Pronouns, Gender, and Preferred Name Advisory Council](https://answers.syr.edu/display/UCC/PGPNAC+-+Pronoun%2C+Gender%2C+and+Preferred+Name+Advisory+Council)__ at Syracuse University. Also at Syracuse University, I have served on the Graduate Diversity and Graduate Employee Committees, the Sexual and Relationship Violence Task Force, the English Undergraduate Committee, the Graduate Student Organization, and the English Graduate Organization. And, yes — I am a _Jeopardy!_ contestant. __[I appeared on TV](../2018/03/02/jeopardy/)__ on March 12, 2018.

I use they/them pronouns; my nickname is "Aley." I am dramaturg and marketing coordinator for __[Syracuse Shakespeare in the Park](https://ssitp.org/)__, where I was assistant director of *Love's Labours Lost* in June 2020. I am a volunteer usher at __[The Landmark Theatre](https://landmarktheatre.org/)__, __[The CNY Playhouse](https://cnyplayhouse.org/)__, and __[Syracuse Stage](https://www.syracusestage.org/)__. I support __[Hunter Hollow Bunny Bed and Breakfast](https://hunterhollow.org/)__. In my free time, I'm often found bicycling by the lake and listening to Mashrou' Leila or Hozier. I enjoy traveling to Montréal and LA, and hope to someday make Washington, DC, my home.

View my __[resume](omara-resume.pdf)__.
My ORCID is __[0000-0003-0540-5376](https://orcid.org/0000-0003-0540-5376)__.
Check out my queer designs on __[Society6](https://society6.com/sarcatholic)__.
Follow me on __[Twitter](https://twitter.com/ashleymomara)__.
